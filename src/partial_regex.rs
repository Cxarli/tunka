use regex_automata::*;

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum PartialRegexState {
	Final,
	Invalid,
	Incomplete,
}

pub trait PartialRegex {
	fn partial_check(&self, buf: &[u8]) -> PartialRegexState;
}

impl PartialRegex for Regex {

	fn partial_check(&self, buf: &[u8]) -> PartialRegexState {
		use PartialRegexState::*;

		// println!("----");

		// Prepare
		let dfa = self.forward();
		let mut state = dfa.start_state();

		// Loop over input
		for &b in buf.iter() {

			let newstate = unsafe { dfa.next_state_unchecked(state, b) };

			// println!("{:?} + {:?} = {:?}", state, b as char, newstate);

			if dfa.is_dead_state(newstate) || newstate == dfa.start_state() {
				return Invalid;
			}

			state = newstate;
		}

		if dfa.is_match_state(state) {
			Final
		} else {
			Incomplete
		}
	}
}
