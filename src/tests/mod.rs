/**
 *  Assert that two Vec's of =='able items are equal
 */
pub fn assert_vec_eq<T: PartialEq + ::std::fmt::Debug>(a: &Vec<T>, b: &Vec<T>) {
	assert!(a.len() == b.len() && b.iter().zip(a).all(|(a, b)| a == b), format!("got {:?} but expected {:?}", a, b));
}

mod expr;
mod simple_parse;
mod simple_lex;

mod test_compile;
mod test_api;

mod test_partial_regex;
