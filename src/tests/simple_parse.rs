use crate::parse::*;
use super::assert_vec_eq;

fn make_regex(txt: &'static str) -> Token { Token::Regex(txt.to_string()) }
fn make_block(txt: &'static str) -> Token { Token::Block(txt.to_string()) }
fn make_literal(txt: &'static str) -> Token { Token::Literal(txt.to_string()) }

#[test]
fn test_regex() {
	let tokens = parse(r#"/ab* c/ { foo() }"#.to_string()).unwrap();
	assert_vec_eq(&tokens, &vec!{ make_regex("ab* c"), make_block("foo()") });
}


#[test]
fn test_literal() {
	let tokens = parse(r#""while" { foo() }"#.to_string()).unwrap();
	assert_vec_eq(&tokens, &vec!{ make_literal("while"), make_block("foo()") });
}


#[test]
fn test_comment() {
	let tokens = parse(r#"
	// this is a
	/a/ {}

	// this is b
	/b/ {}

	// there is no c"#.to_string()).unwrap();
	assert_vec_eq(&tokens, &vec!{ make_regex("a"), make_block(""), make_regex("b"), make_block("") });
}


#[test]
fn test_weird_literal() {
	let tokens = parse(r#""2r0823q/p982ry][q5\4/2y/rq398hadw.wy;4q32r987r;###%#!@$%$^TY^*&%R$%D^%(ED*Fawh;'/][\0-91p7o" {}"#.to_string()).unwrap();
	assert_vec_eq(&tokens, &vec!{ make_literal(r#"2r0823q/p982ry][q5\4/2y/rq398hadw.wy;4q32r987r;###%#!@$%$^TY^*&%R$%D^%(ED*Fawh;'/][\0-91p7o"#), make_block("") });
}

#[test]
fn test_weird_regex() {
	let tokens = parse(r#"/2r0823q"p982ry[q54"2y]\"rq398hadw.wy;4q32r987r;###%#!@$%$^TY^*&%R$%D^%(ED*Faw)*h;['"\0-9]?1p7o/ {}"#.to_string()).unwrap();
	assert_vec_eq(&tokens, &vec!{ make_regex(r#"2r0823q"p982ry[q54"2y]\"rq398hadw.wy;4q32r987r;###%#!@$%$^TY^*&%R$%D^%(ED*Faw)*h;['"\0-9]?1p7o"#), make_block("") });
}


#[test]
fn test_invalid_literal() {
	// escaping isn't supported in literals
	let tokens = parse(r#""we don\"t support escaping"      {}"#.to_string());
	assert_eq!(tokens.unwrap_err(), "expected { but got 't'".to_string());
}

#[test]
fn test_invalid_regex() {
	// escaping isn't supported in regex
	let tokens = parse(r#"/1\/2 == .5/      {}"#.to_string());
	assert_eq!(tokens.unwrap_err(), "expected { but got '2'".to_string());
}


#[test]
fn test_invalid_no_block() {
	let tokens = parse(r#"/[a-z]*/"#.to_string());
	assert_eq!(tokens.unwrap_err(), r#"no block after Regex("[a-z]*")"#.to_string());
}

#[test]
fn test_neverending_literal() {
	let tokens = parse(r#""hello world { foo(); }"#.to_string());
	assert_eq!(tokens.unwrap_err(), "ended within a Literal".to_string());
}

#[test]
fn test_neverending_regex() {
	let tokens = parse(r#"/hello world { foo(); }"#.to_string());
	assert_eq!(tokens.unwrap_err(), "ended within a Regex".to_string());
}


#[test]
fn test_more_rules() {
	let tokens = parse(r#"
	/hello/ { foo() }
	/world/ { bar() }
	"foobar" { foobar() }
	"#.to_string()).unwrap();
	assert_vec_eq(&tokens, &vec!{ make_regex("hello"), make_block("foo()"), make_regex("world"), make_block("bar()"), make_literal("foobar"), make_block("foobar()") });
}
