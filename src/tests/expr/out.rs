#[allow(dead_code)]
#[derive(Debug, PartialEq)]
pub enum MyToken {
	Int(i64),
	Float(f64),

	Plus,
	Minus,
	Asterisk,
	Slash,
	Percent,

	If,
	While,
	For,
	Foreach,

	Gt,
	Ge,
	Lt,
	Le,
	Eq,
	Ne,

	Identifier(String),
}

pub fn mylex(_: String) -> Result<Vec<MyToken>, String> { unimplemented!(); }