use crate::parse::parse;
use crate::lex::*;
use crate::compile::compile;

#[test]
pub fn test_expr_test() {

	// First we parse the syntax file
	let rules = parse(include_str!("test.tnk").to_string()).and_then(lex).expect("Syntax error");

	// Then we compile it into a program and write it to out.rs
	let program = compile(rules).expect("Compile error");
	::std::fs::write("src/tests/expr/out.rs", &program).unwrap();

	// Then we test that it actually works
	// TODO: This has to be tested differently
	let output = ::std::process::Command::new("cargo").args(&["test", "tests::expr::test_expr_test_out", "--", "--ignored", "--nocapture"]).status();

	// Then we overwrite the out.rs file with a boilerplate which is guaranteed to work,
	// otherwise the test file might not be able to compile :/
	// This really has to be tested differently
	::std::fs::write("src/tests/expr/out.rs",
		String::new() + "#[allow(dead_code)]\n" + include_str!("mytoken.rs") + "\npub fn mylex(_: String) -> Result<Vec<MyToken>, String> { unimplemented!(); }"
	).unwrap();

	// Make sure the test was actually successful
	assert_eq!(output.unwrap().code(), Some(0));
}

// Sometimes you'll have to fiddle with this in order to make it work
//*

use super::assert_vec_eq;

mod out;
#[test]
#[ignore]
pub fn test_expr_test_out() {
	use out::{ *, MyToken::* };

	// Simple literals
	assert_vec_eq(&mylex("if while + for * foreach +     if".to_string()).unwrap(), &vec!{ If, While, Plus, For, Asterisk, Foreach, Plus, If });

	// Regexes
	assert_vec_eq(&mylex("123".to_string()).unwrap(), &vec!{ Int(123) });

	// Regexes
	assert_vec_eq(&mylex("123 456".to_string()).unwrap(), &vec!{ Int(123), Int(456) });

	// Complex
	assert_vec_eq(&mylex("if 123 + 456 * 7.89 >= 0.0".to_string()).unwrap(), &vec!{ If, Int(123), Plus, Int(456), Asterisk, Float(7.89), Ge, Float(0.0) });
}

// */
