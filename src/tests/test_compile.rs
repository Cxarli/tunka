/**
 * This file is used to test whether compile_fw_pre.rs and compile_fw_post.rs work on their own
 */


#[derive(Debug, PartialEq)]
pub enum MyToken { }

include!("../compile_fw_pre.rs");

lazy_static! {
	static ref RULES: [Rule; 2] = [
		Rule { matcher: Matcher::new_regex(" "), func: |_| { None } },
		Rule { matcher: Matcher::new_literal(" "), func: |_| { None } },
	];
}

include!("../compile_fw_post.rs");
