use crate::lex::*;
use crate::parse::parse;
use super::assert_vec_eq;

fn make_regex(txt: &'static str) -> Matcher { Matcher::Regex(txt.to_string()) }
fn make_literal(txt: &'static str) -> Matcher { Matcher::Literal(txt.to_string()) }
fn make_rule(m: Matcher, txt: &'static str) -> Rule { Rule { matcher: m, block: txt.to_string() } }


#[test]
fn test_lex() {
	let tokens = parse(r#"
	/hello/ { foo() }
	/world/ { bar() }
	"foobar" { foobar() }
	"#.to_string()).unwrap();

	let blocks = lex(tokens).unwrap();
	assert_vec_eq(&blocks, &vec!{ make_rule(make_regex("hello"), "foo()"), make_rule(make_regex("world"), "bar()"), make_rule(make_literal("foobar"), "foobar()") });
}
