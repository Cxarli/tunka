use crate::tunka_io;
use tempfile::tempfile;
use std::io::{Write, Read, Seek};


#[derive(Debug)]
pub enum Error {
	Io(std::io::Error),
	String(String),
}

impl From<::std::io::Error> for Error { fn from(other: ::std::io::Error) -> Self { Error::Io(other) } }
impl From<String> for Error { fn from(other: String) -> Self { Error::String(other) } }


#[test]
pub fn test_api() -> Result<(), Error> {

	let mut infile = tempfile()?;
	let mut outfile = tempfile()?;

	writeln!(infile, r#"
	/[0-9]+(\.[0-9]*)/   {{ MyToken::Number(r#in) }}
	/"[^"]*"/  {{ MyToken::String(r#in) }}
	/'[^']'/   {{ MyToken::Character(r#in) }}

	"number" {{ MyToken::TNumber }}
	"string" {{ MyToken::TString }}
	"character" {{ MyToken::TCharacter }}
	"#)?;

	infile.flush()?;
	infile.seek(std::io::SeekFrom::Start(0))?;

	tunka_io(&mut infile, &mut outfile)?;

	outfile.flush()?;
	outfile.seek(std::io::SeekFrom::Start(0))?;

	let mut buf = String::new();
	outfile.read_to_string(&mut buf)?;

	println!("{}", buf);

	// @TODO compile and test input

	Ok(())
}
