use regex_automata::Regex;
use crate::partial_regex::*;


#[test]
pub fn test_ra() {
	// So I want to spot the difference between the following cases:
	//
	//    regex    /[0-9]+\.[0-9]+/
	//
	// A)
	//    input    123
	//    result   Incomplete
	//
	// B)
	//    input    abc
	//    result   Invalid
	//
	// C)
	//    input    12.34
	//    result   Final
	//
	// D)
	//    input    12.3abc
	//    result   Invalid
	//
	// E)
	//    input    abc12.3
	//    result   Invalid


	let regex = r"[0-9]+\.[0-9]+";
	let input_a = "123";
	let input_b = "abc";
	let input_c = "12.34";
	let input_d = "12.3abc";
	let input_e = "abc12.3";

	let re = Regex::new(regex).unwrap();

	use PartialRegexState::*;

	assert_eq!(re.partial_check(input_a.as_bytes()), Incomplete);
	assert_eq!(re.partial_check(input_b.as_bytes()), Invalid);
	assert_eq!(re.partial_check(input_c.as_bytes()), Final);
	assert_eq!(re.partial_check(input_d.as_bytes()), Invalid);
	assert_eq!(re.partial_check(input_e.as_bytes()), Invalid);


	assert_eq!(re.partial_check(b""), Incomplete);
}
