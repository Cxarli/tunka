use crate::*;

use std::fs::File;
use std::io::{ Read, Write };

/**
 * Take an input and output file.
 * Parse the input file in the known format, rewrite to list of tokens
 * and write to outfile.
 */
pub fn tunka_io(infile: &mut File, outfile: &mut File) -> Result<(), String> {

	// Read file
	let mut contents = String::new();
	if let Err(err) = infile.read_to_string(&mut contents) {
		return Err(format!("error during reading: {}", err));
	}

	// Parse
	let tokens = parse(contents);
	if let Err(err) = tokens {
		return Err(format!("error during parsing: {}", err));
	}
	let tokens = tokens.unwrap();

	// Lex
	let rules = lex(tokens);
	if let Err(err) = rules {
		return Err(format!("error during lex: {}", err));
	}
	let rules = rules.unwrap();

	// Compile
	let program = compile(rules);
	if let Err(err) = program {
		return Err(format!("error during compile: {}", err));
	}
	let program = program.unwrap();

	// Write file
	if let Err(err) = outfile.write(program.as_bytes()) {
		return Err(format!("error during writing: {}", err));
	}

	// Nothing wrong
	Ok(())
}
