use crate::lex::*;

use regex_automata::Regex;


pub fn compile(rules: Vec<Rule>) -> Result<String, String> {

	let mut program = String::new();

	// Load all user-provided enums etc
	program += include_str!("tests/expr/mytoken.rs");

	program += "\n// ------\n\n";

	// Set up the framework with Rule etc
	program += include_str!("compile_fw_pre.rs");

	program += "\n// ------\n\n";

	// Generate list of rules
	program += &(r#"
lazy_static! {
	static ref RULES: [Rule; "#.to_string() + &rules.len().to_string() + r#"] = [
		#![allow(unused_variables)]
"#);

	for rule in rules.iter() {
		let create_matcher = match &rule.matcher {
			Matcher::Regex(r) => {
				// Make sure regex is valid
				if let Err(e) = Regex::new(r) {
					return Err(e.to_string());
				}

				format!("Matcher::new_regex({:?})", r)
			},
			Matcher::Literal(l) => format!("Matcher::new_literal({:?})", l),
		};

		program += &format!("\t\tRule {{ matcher: {}, func: |r#in| {{ {} }} }},\n", create_matcher, rule.block);
	}

	program += "\t];\n}";

	program += "\n// ------\n\n";

	// Add main function etc
	program += include_str!("compile_fw_post.rs");

	return Ok(program);
}
