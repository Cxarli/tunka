#[derive(Debug, PartialEq, Eq, Clone, Copy)]
enum Location {
	// keep in mind that comments are always at global scope
	// because they are invalid between identifier and block,
	// and because we don't parse the block
	Comment,
	Global,
	Regex,
	Literal,
	BetweenIdentifierAndBlock,
	Block,
}

#[derive(Debug, PartialEq, Eq)]
pub enum Token {
	Literal(String),
	Regex(String),
	Block(String),
}


pub fn parse(input: String) -> Result<Vec<Token>, String> {

	let mut place = Location::Global;
	let mut token: String = "".to_string();

	let mut stack: Vec<Token> = vec!{ };

	for c in input.chars() {
		let (xplace, xtoken) = match (&place, c, token.as_str()) {

			// inside comment: end
			(Location::Comment, '\n', _) => {
				(Location::Global, "".to_string())
			}

			// inside comment: ignore
			(Location::Comment, _, _) => {
				(Location::Comment, "".to_string())
			}

			// inside literal: end
			(Location::Literal, '"', tok) => {
				stack.push(Token::Literal(tok.to_string()));
				(Location::BetweenIdentifierAndBlock, "".to_string())
			}

			// inside literal: add to token
			(Location::Literal, c, token) => {
				let mut s = token.to_string();
				s.push(c);
				(Location::Literal, s)
			}

			// inside regex: end
			(Location::Regex, '/', tok) => {
				stack.push(Token::Regex(tok.to_string()));
				(Location::BetweenIdentifierAndBlock, "".to_string())
			}

			// inside regex: add to token
			(Location::Regex, c, token) => {
				let mut s = token.to_string();
				s.push(c);
				(Location::Regex, s)
			}

			// inside block: end
			(Location::Block, '}', block) => {
				stack.push(Token::Block(block.trim().to_string()));
				(Location::Global, "".to_string())
			}

			// inside block: add to token
			(Location::Block, c, token) => {
				let mut s = token.to_string();
				s.push(c);
				(Location::Block, s)
			}


			// enter comment with //
			(Location::Global, '/', "/") => {
				(Location::Comment, "".to_string())
			}

			// enter regex with /[^/]
			(Location::Global, c, "/") => {
				(Location::Regex, c.to_string())
			}

			// prepare comment
			(Location::Global, '/', "") => {
				(Location::Global, "/".to_string())
			}

			// enter literal with "
			(Location::Global, '"', _) => {
				(Location::Literal, "".to_string())
			}

			// enter block with {
			(Location::BetweenIdentifierAndBlock, '{', _) => {
				(Location::Block, "".to_string())
			}


			// ignore whitespace
			(&p, ' ', t) | (&p, '\t', t) => {
				(p, t.to_string())
			}

			// ignore newline in global
			(Location::Global, '\n', t) => {
				(Location::Global, t.to_string())
			}


			// invalid in global
			(Location::Global, c, t) => {
				return Err(format!("unexpected character '{}' after '{}' at global scope", c, t));
			}

			// invalid in BetweenIdentifierAndBlock
			(Location::BetweenIdentifierAndBlock, c, _) => {
				return Err(format!("expected {{ but got '{}'", c));
			}
		};

		place = xplace;
		token = xtoken;
	}


	// check where we stand
	match place {
		Location::Global | Location::Comment => Ok(stack),

		Location::BetweenIdentifierAndBlock => Err(format!("no block after {:?}", stack.last().unwrap())),

		p => Err(format!("ended within a {:?}", p)),
	}
}
