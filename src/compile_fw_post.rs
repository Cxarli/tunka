const EOF: char = 0 as char;

#[allow(dead_code)]
pub fn mylex(mut input: String) -> Result<Vec<MyToken>, String> {

	let mut tokens = vec!{ };

	let mut token = String::new();
	let mut prevfinal: Option<Vec<&Rule>> = None;

	// Add eof marker
	input.push(EOF);

	let mut input = input.chars().peekable();

	loop {

		// Get next token to be
		let mut nexttoken = token.clone();
		nexttoken.push(*input.peek().unwrap());

		// Check if this next token would be building or final
		let is_build = RULES.iter().filter(|rule| rule.is_build(&nexttoken)).collect::<Vec<&Rule>>();
		let is_final = RULES.iter().filter(|rule| rule.is_final(&nexttoken)).collect::<Vec<&Rule>>();
		let is_end = { let x = input.peek(); x.is_some() && *x.unwrap() == EOF };

		let broken = is_build.is_empty();
		// println!("is_build={:?} is_final={:?} broken={} is_end={} token={:?} ", is_build, is_final, broken, is_end, nexttoken);

		// The new token would break, so we have to reduce the previous token
		if broken || is_end {
			match prevfinal {
				None => return Err(format!("no previous matches")),

				Some(x) if x.is_empty() => return Err(format!("no possible reductions for token '{}'", nexttoken)),

				Some(x) => {

					// More options?
					let index = if x.len() == 1 {
						0
					} else {
						// println!("multiple possible reductions: token is '{}' and has reductions {:?} - will choose first literal if any, otherwise first regex", token, x);

						if let Some(ix) = x.iter().position(|rule| rule.is_literal()) {
							ix
						} else {
							0
						}
					};

					// Reduce string token and reset to empty string
					let tok = (x[index].func)(::std::mem::take(&mut token));

					// Add to tokens if not to be ignored
					if let Some(t) = tok {
						tokens.push(t);
					}
				},
			}
		} else {
			// Consume character and add to token
			token.push(input.next().unwrap());
		}

		// End of input
		if is_end {
			break;
		}

		prevfinal = Some(is_final);
	}

	Ok(tokens)
}
