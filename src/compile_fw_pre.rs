/**
 *
 * THIS FILE WILL BE OVERWRITTEN
 *
 */

extern crate regex_automata;
use regex_automata::Regex;
use crate::partial_regex::{ PartialRegex, PartialRegexState };


enum Matcher {
	Regex(Regex, &'static str),
	Literal(&'static str),
}

impl Matcher {
	pub fn new_regex(s: &'static str) -> Self {
		Matcher::Regex(Regex::new(&format!("{}", s)).unwrap(), s)
	}

	pub fn new_literal(s: &'static str) -> Self {
		Matcher::Literal(s)
	}

	fn is_build(&self, s: &String) -> bool {
		match self {
			Matcher::Regex(ms, _) => ms.partial_check(s.as_bytes()) != PartialRegexState::Invalid,
			Matcher::Literal(ls) => ls.starts_with(s),
		}
	}

	fn is_final(&self, s: &String) -> bool {
		match self {
			Matcher::Regex(ms, _) => ms.partial_check(s.as_bytes()) == PartialRegexState::Final,
			Matcher::Literal(ls) => ls == s,
		}
	}

	fn is_literal(&self) -> bool {
		match self {
			Matcher::Literal(_) => true,
			_ => false,
		}
	}
}

impl PartialEq for Matcher {
	fn eq(&self, other: &Self) -> bool {
		use Matcher::*;

		match (self, other) {
			(Literal(s), Literal(o)) => s == o,
			(Regex(_, s), Regex(_, o)) => s == o,
			(_, _) => false,
		}
	}
}

impl ::std::fmt::Debug for Matcher {
	fn fmt(&self, fmt: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
		use Matcher::*;
		match self {
			Literal(l) => fmt.write_fmt(format_args!("{:?}", l)),
			Regex(_, r) => fmt.write_fmt(format_args!("/{}/", r)),
		}
	}
}


struct Rule {
	matcher: Matcher,
	func: fn(s: String) -> Option<MyToken>,
}

impl Rule {
	fn is_build(&self, s: &String) -> bool { self.matcher.is_build(s) }
	fn is_final(&self, s: &String) -> bool { self.matcher.is_final(s) }
	fn is_literal(&self) -> bool { self.matcher.is_literal() }
}

impl ::std::fmt::Debug for Rule {
	fn fmt(&self, formatter: &mut ::std::fmt::Formatter) -> ::std::fmt::Result { self.matcher.fmt(formatter) }
}

impl PartialEq for Rule {
	fn eq(&self, other: &Self) -> bool { self.matcher == other.matcher }
}
