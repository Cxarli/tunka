use crate::Token;

#[derive(Debug, PartialEq, Eq)]
pub enum Matcher {
	Literal(String),
	Regex(String),
}

#[derive(Debug, PartialEq, Eq)]
pub struct Rule {
	pub matcher: Matcher,
	pub block: String,
}


pub fn lex(mut tokens: Vec<Token>) -> Result<Vec<Rule>, String> {
	let mut matcher = None;

	let mut rules = vec!{ };

	while !tokens.is_empty() {
		let token = tokens.remove(0);

		match (token, matcher.take()) {

			(Token::Literal(x), None) => {
				matcher = Some(Matcher::Literal(x));
			},

			(Token::Regex(x), None) => {
				matcher = Some(Matcher::Regex(x));
			},

			(Token::Block(b), Some(m)) => {
				rules.push(Rule { matcher: m, block: b });
			},

			(t, m) => {
				return Err(format!("Unexpected token {:?} after matcher {:?}", t, m));
			},
		}
	}

	Ok(rules)
}
