#[allow(unused_imports)]
#[macro_use]
extern crate lazy_static;
extern crate regex_automata;

mod partial_regex;
pub use partial_regex::*;

mod parse;
pub use parse::*;

mod lex;
pub use lex::*;

mod compile;
pub use compile::*;

mod easy_api;
pub use easy_api::*;


#[cfg(test)]
mod tests;
