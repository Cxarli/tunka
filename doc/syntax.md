```
QUOT : "
SLASH : /
LBRACE : {
BRACE : }
NEWLINE : \n
SPACE : ' '
TAB : \t


_w_ : SPACE | TAB | NEWLINE
_ws_ : _ws_ _w_ | _w_
_literal_ : QUOT [^"]* QUOT
_regex_ : SLASH [^/]+ SLASH
_block_ : [^}]*
_comment_ : SLASH SLASH [^\n]*
_comments_ : _comments_ _comment_ NEWLINE | _comment_ NEWLINE
_cws_ : _cws_ _comments_ | _cws_ _ws_ | _comments_ | _ws_
_match_ : _literal_ | _regex_
_rule_ : _match_ _ws_ LBRACE _block_ RBRACE
_rules_ : _rules_ _cws_ _rule_ | _rules_ _rule_ | _cws_ _rule_ | _rule_
_start_ : _rules_ _cws_ | _rules_
```


Note: comments between match and block are not allowed on purpose
